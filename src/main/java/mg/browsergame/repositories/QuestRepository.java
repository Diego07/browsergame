package mg.browsergame.repositories;

import mg.browsergame.domain.character.Quest;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface QuestRepository extends CrudRepository<Quest, Long> {
}
