package mg.browsergame.services;


import mg.browsergame.config.PasswordEnc;
import mg.browsergame.domain.character.GameCharacter;
import mg.browsergame.domain.user.User;
import mg.browsergame.exceptions.IllegalUserEmailException;
import mg.browsergame.exceptions.IllegalUsernameException;
import mg.browsergame.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {


    private final PasswordEnc passwordEnc;
    private final UserRepository userRepository;

    @Autowired
    private EntityManager entityManager;

    public UserServiceImpl(PasswordEnc passwordEnc, UserRepository userRepository) {
        this.passwordEnc = passwordEnc;
        this.userRepository = userRepository;
    }

    @Override
    public User saveAndCheckIfUserExists(User user) {

        if (checkIfUsernameExists(user)) {

            throw new IllegalUsernameException(user);

        } else if (checkIfUserEmailExists(user)) {

            throw new IllegalUserEmailException(user);

        } else {

            user.setPassword(passwordEnc.passwordEncoder().encode(user.getPassword()));
            userRepository.save(user);

        }
        return user;
    }

    @Override
    public User saveUser(User user) {

        userRepository.save(user);
        return user;
    }

    private boolean checkIfUsernameExists(User user) {

        User userByUsername = findUserByUsername(user.getUsername());

        return userByUsername != null;
    }

    private boolean checkIfUserEmailExists(User user) {

        User userByEmail = findUserByEmail(user.getEmail());
        return userByEmail != null;
    }

    @Override
    public User findUserByEmail(String email) {

        Query dbquery = entityManager.createQuery("FROM User u WHERE LOWER(u.email) LIKE :email ");
        dbquery.setParameter("email", email.toLowerCase());

        return (User) dbquery.getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public User findUserById(Long id) {

        Optional<User> user = userRepository.findById(id);
        return user.get();
    }

    @Override
    public User findUserByUsername(String name) {

        Query dbquery = entityManager.createQuery("FROM User u WHERE LOWER(u.username) LIKE :name");
        dbquery.setParameter("name", name.toLowerCase());

        return (User) dbquery.getResultList().stream().findFirst().orElse(null);


    }

    @Override
    public User currentlyLoggedUser() {

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String currentPrincipalName = authentication.getName();

        return findUserByUsername(currentPrincipalName);
    }

}


