package mg.browsergame.services;

import mg.browsergame.domain.user.User;

import java.util.Optional;

public interface UserService{

    User saveUser(User user);
    User saveAndCheckIfUserExists(User user);
    User findUserByUsername (String name);
    User findUserByEmail (String email);
    User findUserById (Long id);
    User currentlyLoggedUser();



}
