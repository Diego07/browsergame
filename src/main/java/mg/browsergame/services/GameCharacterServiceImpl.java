package mg.browsergame.services;

import mg.browsergame.domain.character.GameCharacter;
import mg.browsergame.domain.user.User;
import mg.browsergame.repositories.GameCharacterRepository;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import java.util.Optional;

@Service
public class GameCharacterServiceImpl implements GameCharacterService {

    private final GameCharacterRepository gameCharacterRepository;
    private final UserService userService;
    private EntityManager entityManager;

    public GameCharacterServiceImpl(GameCharacterRepository gameCharacterRepository, UserService userService) {
        this.gameCharacterRepository = gameCharacterRepository;
        this.userService = userService;
    }

    @Override
    public GameCharacter findCharById(Long id) {

        Optional<GameCharacter> gameCharacter = gameCharacterRepository.findById(id);
        return gameCharacter.get();
    }

    @Override
    public void saveBootstrapCharToUser() {

        User user = userService.findUserById(1L);
        GameCharacter charById = findCharById(1L);

        charById.setUser(user);
        user.setGameCharacter(charById);

        gameCharacterRepository.save(charById);
        userService.saveUser(user);
    }

    @Override
    public GameCharacter saveCharacterToUser(GameCharacter gameCharacter) {

        User user = userService.currentlyLoggedUser();

        gameCharacter.setUser(user);
        user.setGameCharacter(gameCharacter);

        return gameCharacterRepository.save(gameCharacter);
    }

    @Override
    public boolean checkIfUserCreateChar() {

        try {

            return userService.currentlyLoggedUser().getGameCharacter().getId() != null;

        } catch (NullPointerException e) {

            return false;
        }
    }


//    @Override
//    public GameCharacter findCharByName(String name) {
//
//        Query dbquery = entityManager.createQuery("FROM GameCharacter c WHERE LOWER(c.name) LIKE :name");
//        dbquery.setParameter("name", name.toLowerCase());
//
//        return (GameCharacter) dbquery.getResultList().stream().findFirst().orElse(null);
//    }
//
//    @Override
//    public boolean checkIfCharNameExists(GameCharacter gameCharacter) {
//
//        try {
//
//            GameCharacter charByName = findCharByName(gameCharacter.getName());
//            return charByName != null;
//
//        } catch (NullPointerException e) {
//
//            return false;
//        }
//    }
}
