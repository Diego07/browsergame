package mg.browsergame.services;

import mg.browsergame.domain.character.GameCharacter;

public interface GameCharacterService {

    GameCharacter findCharById (Long id);
    void saveBootstrapCharToUser();
    boolean checkIfUserCreateChar();
    GameCharacter saveCharacterToUser(GameCharacter gameCharacter);
//    boolean checkIfCharNameExists(GameCharacter gameCharacter);
//    GameCharacter findCharByName(String name);

}
