package mg.browsergame.services;

import mg.browsergame.domain.character.Quest;

public interface QuestService {

    Quest saveQuest (Quest quest);
}
