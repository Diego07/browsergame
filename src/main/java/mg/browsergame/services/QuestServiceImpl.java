package mg.browsergame.services;

import mg.browsergame.domain.character.Quest;
import mg.browsergame.repositories.QuestRepository;

import javax.persistence.EntityManager;

public class QuestServiceImpl implements QuestService {

    private final QuestRepository questRepository;

    private EntityManager entityManager;

    public QuestServiceImpl(QuestRepository questRepository) {
        this.questRepository = questRepository;
    }

    @Override
    public Quest saveQuest(Quest quest) {

        return questRepository.save(quest);

    }
}
