package mg.browsergame.controllers;

import mg.browsergame.services.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HomeController {

    private UserService userService;

    public HomeController(UserService userService) {
        this.userService = userService;
    }

    @RequestMapping("/")
    public String homeController() {

        System.out.println(userService.currentlyLoggedUser());

        return "home-view";
    }
}
