package mg.browsergame.controllers;

import mg.browsergame.domain.user.User;
import mg.browsergame.exceptions.IllegalUserEmailException;
import mg.browsergame.exceptions.IllegalUsernameException;
import mg.browsergame.services.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class LoginController {

    private UserService userService;

    public LoginController(UserService userService) {
        this.userService = userService;
    }

    @RequestMapping("/login")
    public String loginPage() {

        return "login-view";
    }

    @RequestMapping
    public String loginError(RedirectAttributes redirectAttributes) {

        redirectAttributes.addFlashAttribute("errorLoginInfo", "Username or password incorrect");
        redirectAttributes.addFlashAttribute("alertClass", "alert-danger");

        return "redirect:/login";
    }

    @RequestMapping("/register")
    public String registerPage(Model model) {

        model.addAttribute("user", new User());

        return "register-view";
    }

    @PostMapping("registerNewUser")
    public String registerNewUser(@ModelAttribute("user") User user, Model model, RedirectAttributes
            redirectAttributes) {

        try {

            userService.saveAndCheckIfUserExists(user);

            model.addAttribute("successReg", "Registration successful! Please log in and create your " +
                    "character!");
            model.addAttribute("alertClass", "alert-success");

            return "login-view";

        } catch (IllegalUsernameException | IllegalUserEmailException e) {

            redirectAttributes.addFlashAttribute("registerError", "Username or email exists" );
            redirectAttributes.addFlashAttribute("alertClass", "alert-danger");

            return "redirect:/register";
        }
    }
}
