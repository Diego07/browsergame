package mg.browsergame.controllers;

import mg.browsergame.domain.character.GameCharacter;
import mg.browsergame.services.GameCharacterService;
import mg.browsergame.services.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class MainCotroller {

    private GameCharacterService gameCharacterService;

    public MainCotroller(GameCharacterService gameCharacterService) {
        this.gameCharacterService = gameCharacterService;
    }

    @RequestMapping("/main")
    public String mainViewOrCreateCharView (Model model) {

        gameCharacterService.saveBootstrapCharToUser();

        if (gameCharacterService.checkIfUserCreateChar()) {

            return "main-view";

        } else {

            model.addAttribute("gameCharacter", new GameCharacter());
            return "createCharacter-view";
        }
    }
}
