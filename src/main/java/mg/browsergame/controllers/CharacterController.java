package mg.browsergame.controllers;

import mg.browsergame.domain.character.GameCharacter;
import mg.browsergame.services.GameCharacterService;
import mg.browsergame.services.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;


@Controller
public class CharacterController {

    private GameCharacterService gameCharacterService;

    public CharacterController(GameCharacterService gameCharacterService) {
        this.gameCharacterService = gameCharacterService;
    }

    @PostMapping("createNewCharacter")
    public String characterCreateView(@ModelAttribute("gameCharacter") GameCharacter gameCharacter) {

            gameCharacterService.saveCharacterToUser(gameCharacter);
            return "main-view";
    }
}
