package mg.browsergame.exceptions;

import mg.browsergame.domain.user.User;

public class IllegalUserEmailException extends RuntimeException {

    public IllegalUserEmailException(User user) {
        System.out.println("Email " + user.getEmail() + " exists");
    }
}
