package mg.browsergame.exceptions;

import mg.browsergame.domain.user.User;

public class IllegalUsernameException extends RuntimeException {

    public IllegalUsernameException(User user) {
        System.out.println("Username: " + user.getUsername() + " exists." );
    }
}
