package mg.browsergame.exceptions;

import mg.browsergame.domain.character.GameCharacter;

public class IllegalCharacterName extends RuntimeException {

    public IllegalCharacterName(GameCharacter gameCharacter) {
        System.out.println("Character named " + gameCharacter.getName() + " + exists");
    }
}
