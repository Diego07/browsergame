package mg.browsergame.bootstrap;

import mg.browsergame.config.PasswordEnc;
import mg.browsergame.domain.character.GameCharacter;
import mg.browsergame.domain.character.Quest;
import mg.browsergame.domain.user.User;
import mg.browsergame.domain.character.CharacterClass;
import mg.browsergame.domain.character.Race;
import mg.browsergame.repositories.GameCharacterRepository;
import mg.browsergame.repositories.QuestRepository;
import mg.browsergame.repositories.UserRepository;
import mg.browsergame.services.GameCharacterService;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Component
public class UserBootstrap implements ApplicationListener<ContextRefreshedEvent> {

    private final UserRepository userRepository;
    private final GameCharacterRepository gameCharacterRepository;
    private final QuestRepository questRepository;
    private final PasswordEnc passwordEnc;

    public UserBootstrap(UserRepository userRepository, GameCharacterRepository gameCharacterRepository, PasswordEnc passwordEnc, GameCharacterService gameCharacterService, QuestRepository questRepository) {
        this.userRepository = userRepository;
        this.gameCharacterRepository = gameCharacterRepository;
        this.passwordEnc = passwordEnc;
        this.questRepository = questRepository;
    }

    @Override
    @Transactional
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        userRepository.saveAll(bootstrapUserList());
        gameCharacterRepository.saveAll(listGameCharacter());
        questRepository.saveAll(listOfQuests());
    }

    private List<User> bootstrapUserList() {

        List<User> userList = new ArrayList<>(2);

        User user = new User();
        user.setEmail("user@gmail.com");
        user.setUsername("user");
        user.setPassword(passwordEnc.passwordEncoder().encode("user"));
        user.setRole("USER");

        userList.add(user);

        User admin = new User();
        admin.setEmail("admin@gmail.com");
        admin.setUsername("admin");
        admin.setPassword(passwordEnc.passwordEncoder().encode("admin"));
        admin.setRole("ADMIN");

        userList.add(admin);

        return userList;
    }

    private List<GameCharacter> listGameCharacter() {

        List<GameCharacter> charList = new ArrayList<>(1);

        GameCharacter gameCharacter = new GameCharacter();
        gameCharacter.setName("Mon");
        gameCharacter.setRace(Race.ELF);
        gameCharacter.setCharacterClass(CharacterClass.MAGE);

        charList.add(gameCharacter);

        return charList;
    }

    private List<Quest> listOfQuests() {

        List<Quest> questList = new ArrayList<>(3);

        Quest dragonQuest = new Quest();
        dragonQuest.setNameOfQuest("Polowanie na smoki");

        Quest huntingQuest = new Quest();
        huntingQuest.setNameOfQuest("Polowanie na dziki");

        Quest collectPotatoQuest = new Quest();
        collectPotatoQuest.setNameOfQuest("Zbieranie ziemniaków");

        questList.add(dragonQuest);
        questList.add(huntingQuest);
        questList.add(collectPotatoQuest);

        return questList;
    }
}
