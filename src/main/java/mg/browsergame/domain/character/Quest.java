package mg.browsergame.domain.character;

import mg.browsergame.component.TimeComponent;

import javax.persistence.*;

@Entity
public class Quest {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String nameOfQuest;

    private int reward = 100;

    private int lengthInSeconds = 10;

    private boolean started = false;

    private boolean completed = false;

    @Embedded
    private TimeComponent timeComponent;

    @OneToOne
    private GameCharacter gameCharacter;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNameOfQuest() {
        return nameOfQuest;
    }

    public void setNameOfQuest(String nameOfQuest) {
        this.nameOfQuest = nameOfQuest;
    }

    public int getReward() {
        return reward;
    }

    public void setReward(int reward) {
        this.reward = reward;
    }

    public int getLengthInSeconds() {
        return lengthInSeconds;
    }

    public void setLengthInSeconds(int lengthInSeconds) {
        this.lengthInSeconds = lengthInSeconds;
    }

    public boolean isStarted() {
        return started;
    }

    public void setStarted(boolean started) {
        this.started = started;
    }

    public boolean isCompleted() {
        return completed;
    }

    public void setCompleted(boolean completed) {
        this.completed = completed;
    }

    public TimeComponent getTimeComponent() {
        return timeComponent;
    }

    public void setTimeComponent(TimeComponent timeComponent) {
        this.timeComponent = timeComponent;
    }
}
